﻿#include <iostream>
using namespace std;



class Animal
{
public:

    Animal(){}

    virtual void voice() 
    {
        cout << "This is basic class";
    }
    virtual ~Animal(){}
};

class Dog : public Animal
{
public:
    
    Dog() {}

    void voice() override
    {
        cout << "Woof!\n";
    }
};

class Cat : public Animal
{
public:

    Cat() {}

    void voice() override
    {
        cout << "Meow!\n";
    }
};

class Cow : public Animal
{
public:

    Cow() {}

    void voice() override
    {
        cout << "Moo!\n";
    }
};



int main()
{
    Animal* animal [3];
    animal[0] = new Dog();
    animal[1] = new Cat();
    animal[2] = new Cow();
    for (Animal* a : animal)
    {
        a->voice();
        delete a;
    }
}